<?php
date_default_timezone_set('America/Sao_Paulo');

include_once "util/class/riotapi.php";



class Parser{

	public function parse($dados){
		$result = json_decode($dados, true);
		return $result;
	}
	

	public function GetSummonerIdByName($summonerName){
		@include ("/var/www/html/hazger/util/extra/connection.php"); //caminho hard coded pra funcionar no servidor agora como que vou fazer pra funcionar local eu nao sei
		@include ("/util/extra/connection.php");

		//echo($mysqli->host_info);
		//echo("AQUI OHH");

		$sql = "SELECT id
				FROM summoner 
				WHERE name = '".$summonerName."'				
		";
		
		$result = $mysqli->query($sql);
		if ($result->num_rows != 0) {
			//echo "Resultado ja no banco de dados <br>";
			$row = $result->fetch_assoc();
			//echo("retornou sem fazer request <br>");
			return $row['id'];
		}else{
			//echo("resultado não ta no banco de dados");
			$riotApi = new RiotApi('br');
			$result = $riotApi->getSummonerByName($_GET["summonerName"]);
			$result = json_decode($result, true);
			
			//var_dump($result[$summonerName]);
			
			$sql = "INSERT INTO summoner (id, name)
					VALUES ('".$result[$summonerName]["id"]."', '".$_GET["summonerName"]."')
			";
			//echo($sql);
			if(!$result[$summonerName]["id"]){
				echo("NOME EM BUGADO DA PORRA");
			}else{
				$mysqli->query($sql);
				$mysqli->commit();
			}
			return $result[$summonerName]["id"];			
		}		
	}


	public function parseDadosPartida($jogos){
		$result = json_decode($jogos, true);
		//echo("Dados das ultimas partidas desse jogador <br>");
		$i = 0;
		$partida;
		//Percorre os dados da partida e guarda na variavel partida[x] o id do jogo do campeao e do summoner e os tipos dos jogos
		foreach ($result["games"] as $dados) {
			$data = date('Y-m-d H:i:s', ($dados["createDate"])/1000);			
			$partida[$i] = array(
							"gameId" 							=> $dados["gameId"],
							"summonerId" 						=> $result["summonerId"],
							"championId" 						=> $dados["championId"],
							"gameMode"   						=> $dados["gameMode"],
							"gameType"   						=> $dados["gameType"],
							"subType"   						=> $dados["subType"],
							"createDate"						=> date('Y-m-d H:i:s', ($dados["createDate"])/1000),
							"win"								=> (@$dados["stats"]["win"] ? '1' : '0') ?: 'NULL',
							"championsKilled"					=> (@$dados["stats"]["championsKilled"]) ?: 'NULL',
							"championId"						=> (@$dados["championId"]) ?: 'NULL',
							"numDeaths"							=> (@$dados["stats"]["numDeaths"]) ?: 'NULL',
							"assists"							=> (@$dados["stats"]["assists"]) ?: 'NULL',
							"teamId"							=> (@$dados["stats"]["team"]) ?: 'NULL',
							"level"								=> (@$dados["stats"]["level"]) ?: 'NULL',
							"timePlayed"						=> (@$dados["stats"]["level"]) ?: 'NULL',
							"minionsKilled"						=> (@$dados["stats"]["minionsKilled"]) ?: 'NULL',
							"neutralMinionsKilled"				=> (@$dados["stats"]["neutralMinionsKilled"]) ?: 'NULL',
							"neutralMinionsKilledEnemyJungle"	=> (@$dados["stats"]["neutralMinionsKilledEnemyJungle"]) ?: 'NULL',
							"neutralMinionsKilledYourJungle"	=> (@$dados["stats"]["neutralMinionsKilledYourJungle"]) ?: 'NULL',
							"goldEarned"						=> (@$dados["stats"]["goldEarned"]) ?: 'NULL',
							"goldSpent"							=> (@$dados["stats"]["goldSpent"]) ?: 'NULL',
							"item0"								=> (@$dados["stats"]["item0"]) ?: 'NULL',
							"item1"								=> (@$dados["stats"]["item1"]) ?: 'NULL',
							"item2"								=> (@$dados["stats"]["item2"]) ?: 'NULL',
							"item3"								=> (@$dados["stats"]["item3"]) ?: 'NULL',
							"item4"								=> (@$dados["stats"]["item4"]) ?: 'NULL',
							"item5"								=> (@$dados["stats"]["item5"]) ?: 'NULL',
							"item6"								=> (@$dados["stats"]["item6"]) ?: 'NULL',
							"firstBlood"						=> (@$dados["stats"]["firstBlood"]) ?: 'NULL',
							"doubleKills"						=> (@$dados["stats"]["doubleKills"]) ?: 'NULL',
							"tripleKills"						=> (@$dados["stats"]["tripleKills"]) ?: 'NULL',
							"quadraKills"						=> (@$dados["stats"]["quadraKills"]) ?: 'NULL',
							"pentaKills"						=> (@$dados["stats"]["pentaKills"]) ?: 'NULL',
							"killingSprees"						=> (@$dados["stats"]["killingSprees"]) ?: 'NULL',
							"largestKillingSpree"				=> (@$dados["stats"]["largestKillingSpree"]) ?: 'NULL',
							"largestMultiKill"					=> (@$dados["stats"]["largestMultiKill"]) ?: 'NULL',
							"totalDamageDealt"					=> (@$dados["stats"]["totalDamageDealt"]) ?: 'NULL',
							"totalDamageDealtToChampions"		=> (@$dados["stats"]["totalDamageDealtToChampions"]) ?: 'NULL',
							"totalDamageTaken"					=> (@$dados["stats"]["totalDamageTaken"]) ?: 'NULL',
							"totalHeal"							=> (@$dados["stats"]["totalHeal"]) ?: 'NULL',
							"totalUnitsHealed"					=> (@$dados["stats"]["totalUnitsHealed"]) ?: 'NULL',
							"totalTimeCrowdControlDealt"		=> (@$dados["stats"]["totalTimeCrowdControlDealt"]) ?: 'NULL',
							"largestCriticalStrike"				=> (@$dados["stats"]["largestCriticalStrike"]) ?: 'NULL',
							"magicDamageDealtPlayer"			=> (@$dados["stats"]["magicDamageDealtPlayer"]) ?: 'NULL',
							"magicDamageDealtToChampions"		=> (@$dados["stats"]["magicDamageDealtToChampions"]) ?: 'NULL',
							"magicDamageTaken"					=> (@$dados["stats"]["magicDamageTaken"]) ?: 'NULL',
							"physicalDamageDealtPlayer"			=> (@$dados["stats"]["physicalDamageDealtPlayer"]) ?: 'NULL',
							"physicalDamageDealtToChampions"	=> (@$dados["stats"]["physicalDamageDealtToChampions"]) ?: 'NULL',
							"physicalDamageTaken"				=> (@$dados["stats"]["physicalDamageTaken"]) ?: 'NULL',
							"trueDamageDealtPlayer"				=> (@$dados["stats"]["trueDamageDealtPlayer"]) ?: 'NULL',
							"trueDamageDealtToChampions"		=> (@$dados["stats"]["trueDamageDealtToChampions"]) ?: 'NULL',
							"trueDamageTaken"					=> (@$dados["stats"]["trueDamageTaken"]) ?: 'NULL',
							"barracksKilled"					=> (@$dados["stats"]["barracksKilled"]) ?: 'NULL',
							"turretsKilled"						=> (@$dados["stats"]["turretsKilled"]) ?: 'NULL',
							"sightWardsBought"					=> (@$dados["stats"]["sightWardsBought"]) ?: 'NULL',
							"visionWardsBought"					=> (@$dados["stats"]["visionWardsBought"]) ?: 'NULL',
							"wardKilled"						=> (@$dados["stats"]["wardKilled"]) ?: 'NULL',
							"wardPlaced"						=> (@$dados["stats"]["wardPlaced"]) ?: 'NULL'
				);
			$i++;			
		}
		return $partida;
	}
	public function parseGames($jogos){
		$result = json_decode($jogos, true);
		//echo("OLA TO AQUI<br>");

		//var_dump($result["games"]);

		
		foreach($result["games"] as $partida) {
		    //var_dump($partida);
		    //cada $key agora é um jogo
		    /*
		    foreach($partida as $info){
		    	echo $info;
		    	echo("<br>");
		    }
		    */

			
		    echo("Id da partida:".$partida["gameId"]."<br>");
		    echo("Invalid flag: ");
		    echo $partida["invalid"] ? 'true' : 'false';
		    echo("<br>");
		    echo("Game mode. (legal values: CLASSIC, ODIN, ARAM, TUTORIAL, ONEFORALL, FIRSTBLOOD):".$partida["gameMode"]."<br>");
		    echo("Game type. (legal values: CUSTOM_GAME, MATCHED_GAME, TUTORIAL_GAME):".$partida["gameType"]."<br>");
		    echo("Game sub-type. (legal values: NONE, NORMAL, BOT, RANKED_SOLO_5x5, RANKED_PREMADE_3x3, RANKED_PREMADE_5x5, ODIN_UNRANKED, RANKED_TEAM_3x3, RANKED_TEAM_5x5, NORMAL_3x3, BOT_3x3, CAP_5x5, ARAM_UNRANKED_5x5, ONEFORALL_5x5, FIRSTBLOOD_1x1, FIRSTBLOOD_2x2, SR_6x6, URF, URF_BOT): ".$partida["subType"]."<br>");
		    echo("Map ID: ".$partida["mapId"]."<br>");
		    echo("Team ID associated with game. Team ID 100 is blue team. Team ID 200 is purple team: ".$partida["teamId"]."<br>");		    
		    echo("Champion ID associated with game: ".$partida["championId"]."<br>");
		    echo("ID of first summoner spell: ".$partida["spell1"]."<br>");
		    echo("ID of second summoner spell: ".$partida["spell2"]."<br>");
		    echo("Level: ".$partida["level"]."<br>");
		    echo("IP Earned: ".$partida["ipEarned"]."<br>");
		    echo("Date that end game data was recorded, specified as epoch milliseconds: ".$partida["createDate"]."<br>");
		    echo("<br>");

		    echo("Outros jogadores da partida<br>");
		    foreach($partida["fellowPlayers"] as $fellowPlayers){
		    	echo("&nbsp;&nbsp;&nbsp;&nbsp; Summoner id associated with player: ".$fellowPlayers["summonerId"]."<br>");
		    	echo("&nbsp;&nbsp;&nbsp;&nbsp; Team id associated with player: ".$fellowPlayers["teamId"]."<br>");
		    	echo("&nbsp;&nbsp;&nbsp;&nbsp; Champion id associated with player: ".$fellowPlayers["championId"]."<br>");
		    	echo("<br>");
		    }
		    echo("Fim dos outros jogadores da partida<br>");
		    //var_dump($partida["fellowPlayers"]);

		    //var_dump($partida["stats"]);
		    echo("comeco dos stats<br>");
		    
		    echo("Assistencias: ".$partida["stats"]["assists"]."<br>");
		    echo("Number of enemy inhibitors killed: ".$partida["stats"]["barracksKilled"]."<br>");
		    echo("championsKilled: ".$partida["stats"]["championsKilled"]."<br>");
		    echo("level: ".$partida["stats"]["level"]."<br>");
		    echo("goldEarned: ".$partida["stats"]["goldEarned"]."<br>");
		    echo("numDeaths: ".$partida["stats"]["numDeaths"]."<br>");
		    echo("turretsKilled: ".$partida["stats"]["turretsKilled"]."<br>");
		    echo("minionsKilled: ".$partida["stats"]["minionsKilled"]."<br>");
		    echo("goldSpent: ".$partida["stats"]["goldSpent"]."<br>");
		    echo("totalDamageDealt: ".$partida["stats"]["totalDamageDealt"]."<br>");
		    echo("totalDamageTaken: ".$partida["stats"]["totalDamageTaken"]."<br>");
		    echo("doubleKills: ".$partida["stats"]["doubleKills"]."<br>");
		    echo("tripleKills: ".$partida["stats"]["tripleKills"]."<br>");
		    echo("killingSprees: ".$partida["stats"]["killingSprees"]."<br>");
		    echo("largestKillingSpree: ".$partida["stats"]["largestKillingSpree"]."<br>");
		    echo("team: ".$partida["stats"]["team"]."<br>");
		    echo("win(boolean depois trata ele): ".$partida["stats"]["win"]."<br>");
		    echo("neutralMinionsKilled: ".$partida["stats"]["neutralMinionsKilled"]."<br>");
		    echo("largestMultiKill: ".$partida["stats"]["largestMultiKill"]."<br>");
		    echo("physicalDamageDealtPlayer: ".$partida["stats"]["physicalDamageDealtPlayer"]."<br>");
		    echo("magicDamageDealtPlayer: ".$partida["stats"]["magicDamageDealtPlayer"]."<br>");
		    echo("physicalDamageTaken: ".$partida["stats"]["physicalDamageTaken"]."<br>");
		    echo("magicDamageTaken: ".$partida["stats"]["magicDamageTaken"]."<br>");
		    echo("largestCriticalStrike: ".$partida["stats"]["largestCriticalStrike"]."<br>");
		    echo("timePlayed: ".$partida["stats"]["timePlayed"]."<br>");
		    echo("totalHeal: ".$partida["stats"]["totalHeal"]."<br>");
		    echo("item0: ".$partida["stats"]["item0"]."<br>");
		    echo("item1: ".$partida["stats"]["item1"]."<br>");
		    echo("item2: ".$partida["stats"]["item2"]."<br>");
		    echo("item3: ".$partida["stats"]["item3"]."<br>");
		    echo("item4: ".$partida["stats"]["item4"]."<br>");
		    echo("item5: ".$partida["stats"]["item5"]."<br>");
		    echo("item6: ".$partida["stats"]["item6"]."<br>");
		    echo("magicDamageDealtToChampions: ".$partida["stats"]["magicDamageDealtToChampions"]."<br>");
		    echo("physicalDamageDealtToChampions: ".$partida["stats"]["physicalDamageDealtToChampions"]."<br>");
		    echo("totalDamageDealtToChampions: ".$partida["stats"]["totalDamageDealtToChampions"]."<br>");
		    echo("trueDamageDealtPlayer: ".$partida["stats"]["trueDamageDealtPlayer"]."<br>");
		    echo("trueDamageDealtToChampions: ".$partida["stats"]["trueDamageDealtToChampions"]."<br>");
		    echo("trueDamageTaken: ".$partida["stats"]["trueDamageTaken"]."<br>");
		    echo("wardPlaced: ".$partida["stats"]["wardPlaced"]."<br>");
		    echo("neutralMinionsKilledEnemyJungle: ".$partida["stats"]["neutralMinionsKilledEnemyJungle"]."<br>");
		    echo("neutralMinionsKilledYourJungle: ".$partida["stats"]["neutralMinionsKilledYourJungle"]."<br>");
		    echo("totalTimeCrowdControlDealt: ".$partida["stats"]["totalTimeCrowdControlDealt"]."<br>");
		    echo("FIM dos stats<br>");

		    echo("<br><br><br>");
		}
		
		

	}
}

?>