<?php
define("DB_SERVIDOR", "localhost");
define("DB_USUARIO", "hazger");
define("DB_PASS", "123senhadobancodedados123");
define("DB_SCHEMA", "hazger");
	class Database{
		public  $mysqli = null;

		//construtor
		public function __construct(){
			$this->mysqli = new mysqli(DB_SERVIDOR, DB_USUARIO, DB_PASS, DB_SCHEMA);
			if ($this->mysqli->connect_errno) {
				echo "Error MySQLi: (". $this->mysqli->connect_errno . ") " . $this->mysqli->connect_error;
				exit();
			}
			$this->mysqli->set_charset("utf8");			
		}

		//destructor??? nunca lembro o nome dessa coisa
		public function __destruct(){
			$this->closeDB();
		}

		//fecha a conexao
		public function closeDB(){
			$this->mysqli->close();
		}

		//faz uma query e retorna ela
		public function runQuery($sql){
			$result = $this->mysqli->query($sql);
			return $result;
		}
	}
?>