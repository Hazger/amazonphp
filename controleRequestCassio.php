<?php
	include("requestCassio.php");

	$request = new Request;
	$user = strtolower($_POST["user"]);
	$summonerID = $request->getSummonerID($user);
	$summonerInfo = $request->getSummonerInfo($summonerID);

	if($summonerInfo != null){
		echo "Summoner Name: ".$summonerInfo["entries"]["playerOrTeamName"]."<br /><br />";
		
		switch($summonerInfo["queue"]){
			case "RANKED_SOLO_5x5":
				echo "Queue: Ranked Solo 5x5<br />"; break;
			case "RANKED_TEAM_3x3":
				echo "Queue: Ranked Team 3x3<br />"; break;
			case "RANKED_TEAM_5x5":
				echo "Queue: Ranked Team 5x5<br />"; break;
		}
		echo "Elo: ".$summonerInfo["tier"]." ".$summonerInfo["entries"]["division"]."<br />";
		echo "League Points: ".$summonerInfo["entries"]["leaguePoints"]."<br />";
		echo "Wins: ".$summonerInfo["entries"]["wins"]."<br />";
	}
	else{
		echo "Invocador \"".$user."\" nao esta associado a uma liga ranqueada.";
	}
?>