<?php 

include_once "classes/class.groups.php";
include_once "classes/class.page.php";

Page::start_session();

$group = new Groups();

switch($_GET['code']){
	case 1: 
		$group->AddNewGroup($_POST['group_name'], $_SESSION['id_user']);
		break;
	case 2:
		$group_id = $group->getGroupID($_GET['group_name']);
		$group->AddNewSummoner($group_id, $_POST['summoner_name']);
		break;
}

header("Location: manage_groups.php");

/*
		$tableName = strtolower(trim(preg_replace( '/[`^~\'"]/', null, iconv( 'UTF-8', 'ASCII//TRANSLIT', $_POST['groupname']))));
		$characteres = array();
		for($i=0; $i<=127; $i++){
			if(($i<48 || $i>57 && $i<97 || $i>122) && $i <> 32)
			array_push($characteres, chr($i));
		}
		$tableName = "t_".str_replace(" ", "_", str_replace($characteres, "", $tableName));

		echo $tableName;
*/

?>

