<?php
Class Database{
	public $mysqli;

	function __construct()
	{
		$this->mysqli = new mysqli(
							"localhost", //endereco
							"CassioMello", //usuario CassioMello
							"gF-kV2x7", //senha gF-kV2x7
							"lolstats_db" //schema
							);
	}

	function DB_insertNewGroup($group_name, $id_user){
		$sql = "INSERT INTO `t_groups` (`id_user`, `group_name`) VALUES (".$id_user.", '".$group_name."')";
		$query_result = mysqli_query($this->mysqli, $sql);
		if(!$query_result)
			return mysqli_error($this->mysqli);
		else
			return true;
	}

	function DB_getGroupID($GroupName){
		$sql = "SELECT `id_group` FROM `t_groups` WHERE `group_name` = '".$GroupName."'";
		$query_result = mysqli_query($this->mysqli, $sql);
		$row = mysqli_fetch_assoc($query_result);
		return $row['id_group'];
	}

	function DB_selectAllGroups($id_user){
		$sql = "SELECT `group_name`, `id_group` FROM `t_groups` WHERE `id_user` = '".$id_user."'";
		$query_result = mysqli_query($this->mysqli, $sql);
		if($query_result != false){
			$num_row = mysqli_num_rows($query_result);
			if ($num_row > 0) {
				$i=0;
				while($row = mysqli_fetch_assoc($query_result)){
					foreach ($row as $campo => $valor) {
						$valores[$i][$campo] = $row[$campo];
					}
					$i++;
				}
				return $valores;
			}
			else{
				return null;
			}
		}
		else{
			return mysqli_error($this->mysqli);
		}
	}

	function DB_selectAllSummoners($id_group){
		$sql = "SELECT `summoner_name`, `id_summoner` FROM `t_summoners` WHERE `id_group` = '".$id_group."'";
		$query_result = mysqli_query($this->mysqli, $sql);
		if($query_result != false){
			$num_row = mysqli_num_rows($query_result);
			if ($num_row > 0) {
				$i=0;
				while($row = mysqli_fetch_assoc($query_result)){
					foreach ($row as $campo => $valor) {
						$valores[$i][$campo] = $row[$campo];
					}
					$i++;
				}
				return $valores;
			}
			else{
				return null;
			}
		}
		else{
			return mysqli_error($this->mysqli);
		}
	}

	function DB_AddNewSummoner($group_id, $summonerName, $summoner_id){
		$sql = "INSERT INTO `t_summoners` (`id_group`, `summoner_name`, `id_summoner`) VALUES ( '".$group_id."', '".$summonerName."', '".$summoner_id."' );";
		$query_result = mysqli_query($this->mysqli, $sql);
		if(!$query_result)
			return mysqli_error($this->mysqli);
		else
			return true;		
	}

	function selectAllUsers(){
		$sql = "SELECT * FROM t_users";
		$query_result = mysqli_query($this->mysqli, $sql);
		if($query_result != false){
			$num_row = mysqli_num_rows($query_result);
			if ($num_row > 0) {
				$i=0;
				while($row = mysqli_fetch_assoc($query_result)){
					foreach ($row as $campo => $valor) {
						$valores[$i][$campo] = $row[$campo];
					}
					$i++;
				}
				return $valores;
			}
			else{
				return null;
			}
		}
		else{
			return mysqli_error($this->mysqli);
		}
	}

	function insertNewUser($username, $password)
	{
		$sql = "INSERT INTO `t_users` (`username`, `password`) VALUES ('" .$username. "','" .$password. "')";
		$error = mysqli_query($this->mysqli, $sql);
		if($error != false)
			return true;
		else
			return mysqli_error($this->mysqli);
	}
}


?>