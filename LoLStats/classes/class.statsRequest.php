<?php

include_once "classes/class.riotapi.php";

Class statsRequest{

	private $riotapi;

	public function __construct(){
		$this->riotapi = new RiotApi('br');
	}

	function getMatchesStatus($summoner_id){		
		$array_matches = $this->riotapi->getGame($summoner_id);
		$array_matches = json_decode($array_matches, true);

		$i = 0;
		foreach ($array_matches['games'] as $matches) {
			$array_winmatches[$i] = $matches['stats']['win'];
			$i++;
		}

		return $array_winmatches;
	}
}