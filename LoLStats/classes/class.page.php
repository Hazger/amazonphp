<?php 

class Page{	  

	public static function start_session(){
		session_start();
	}

	public static function getHeader($nomePagina){

	 	Page::start_session();
	 	if (!isset($_SESSION)) {   
	  		$_SESSION['logged'] = false;
	 	}

		if($_SESSION['logged']){
			$menu_login = '<li><a href="logout.php">'.$_SESSION['username'].'</a></li>
						   <li><a href="logout.php">Logout</a></li>
						  ';
			$menu_manage = '<li><a href="manage_groups.php">Manage Groups</a></li>
							<li><a href="stats.php">Stats</a></li>
							';
		}
		else{
			$menu_login = '<li><a href="login.php">Login</a></li>
					 <li><a href="new_account.php">Create New Account</a></li>
					';
			$menu_manage = "";
		}

		$header = '
		<!DOCTYPE html>
		<html lang="en">
		<head>
			<meta charset="utf-8">
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<title>'.$nomePagina.'</title>

			<!-- CSS -->
				<link rel="stylesheet" href="css/bootstrap.min.css">
				<link rel="stylesheet" href="css/bootstrap.css">
				<link rel="stylesheet" href="css/style.css">
				<link rel="stylesheet" href="css/stats_menu.css">

			<script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
		</head>
		<body>

		<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="index.php">LoL Stats</a>
				</div>
				<div class="collapse navbar-collapse">
					<ul class="nav navbar-nav">
						<li class="active"><a href="index.php">Home</a></li>
						<li><a href="about.php">About</a></li>'
						.$menu_manage.
						'<li><a href="contact">Contact</a></li>
					</ul>
					<ul class="nav navbar-nav navbar-right">'
						.$menu_login.
					'</ul>
				</div><!--/.nav-collapse -->
			</div>
		</div>
		';
		echo $header;
	}

	public static function getFooter(){
		$footer =
		'
		<!-- jQuery (necessary for Bootstraps JavaScript plugins) -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="js/bootstrap.min.js"></script>
		</body>
		</html>
		';
		echo $footer;
	}
} //fim da classe Pagina

?>