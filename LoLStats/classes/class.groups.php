<?php 

include_once "classes/class.database.php";
include_once "classes/class.riotapi.php";

Class Groups{
	private $db;
	private $riotapi;

	function __construct()
	{
		$this->db = new Database();
		$this->riotapi = new Riotapi('br');
	}

	function AddNewGroup($groupName, $id_user){
		$return_status = $this->db->DB_insertNewGroup($groupName, $id_user);
		if($return_status === true)
			return true;
		else
			return $return_status;
	}

	function AddNewSummoner($group_id, $summonerName){
		$summoner_id = $this->riotapi->getSummonerID($summonerName);
		$return_status = $this->db->DB_AddNewSummoner($group_id, $summonerName, $summoner_id);
		if($return_status === true)
			return true;
		else
			return $return_status;
	}

	function getGroupID($GroupName){
		$query_result = $this->db->DB_getGroupID($GroupName);
		return $query_result;			
	}

	function getAllGroups($id_user){
		$query_result = $this->db->DB_selectAllGroups($id_user);
		return $query_result;
	}

	function getAllSummoners($id_group){
		$query_result = $this->db->DB_selectAllSummoners($id_group);
		return $query_result;		
	}
}
?>
