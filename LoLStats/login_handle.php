<?php 

include_once "classes/class.page.php";
include_once "classes/class.database.php";

page::getHeader("LoL Stats");

$db = new Database();

$results = $db->selectAllUsers();

$_SESSION['logged'] = false;

if($results != null){
	foreach($results as $table_line){
		foreach($table_line as $column => $values){
			if($table_line['username'] == $_POST['username'] AND $table_line['password'] == $_POST['password']){
				$_SESSION['logged'] = true;
				$_SESSION['username'] = $table_line['username'];
				$_SESSION['id_user'] = $table_line['id_user'];
				break;
			}
		}	
	}
}
header("Location: index.php");

page::getFooter();

?>