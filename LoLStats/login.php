<?php 
include_once "classes/class.page.php";

page::getHeader("LoL Stats");
?>

<br><br><br><br>

<div class="container">

	<form class="form-signin" role="form" action="login_handle.php" method="post">
		<h2 class="form-signin-heading">Please sign in</h2>
		<input type="text" class="form-control" placeholder="Username" name="username" required autofocus>
		<input type="password" class="form-control" placeholder="Password" name="password" required>
		<label class="checkbox">
			<input type="checkbox" value="remember-me"> Remember me
		</label>
		<button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
	</form>

</div>

<?php
page::getFooter();
?>