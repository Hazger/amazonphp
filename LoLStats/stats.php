<?php 

include_once "classes/class.page.php";
include_once "classes/class.database.php";
include_once "classes/class.groups.php";

page::getHeader("LoL Stats");
echo "<br>";
?>

<!--Load the AJAX API-->
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">

  // Load the Visualization API library and the piechart library.
  	google.load('visualization', '1.0', {'packages':['corechart']});
  	google.setOnLoadCallback(initChart);

$(document).ready(function(){

  	$('#cssmenu > ul > li ul').each(function(index, element){
  		var count = $(element).find('li').length;
  		var content = '<span class="cnt">' + count + '</span>';
  		$(element).closest('li').children('a').append(content);
  	});

  	$('#cssmenu ul ul li:odd').addClass('odd');
  	$('#cssmenu ul ul li:even').addClass('even');

  	$('#cssmenu > ul > li > a').click(function() {

  		var checkElement = $(this).next();

  		$('#cssmenu li').removeClass('active');
  		$(this).closest('li').addClass('active'); 

  		if((checkElement.is('ul')) && (checkElement.is(':visible'))) {
  			$(this).closest('li').removeClass('active');
  			checkElement.slideUp('normal');
  		}
  		if((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
  			$('#cssmenu ul ul:visible').slideUp('normal');
  			checkElement.slideDown('normal');
  		}

  		if($(this).closest('li').find('ul').children().length == 0) {
  			return true;
  		} else {
  			return false; 
  		}
  	});

  	$('.menu_itens').click(function() {
  		if($(this).hasClass('odd'))
  			$(this).removeClass('odd');
  		if($(this).hasClass('even'))
  			$(this).removeClass('even');
  		if($(this).hasClass('selected')){
  			$(this).removeClass('selected');
  		}
  		else
  			$(this).addClass('selected');
  	});

  });

function initChart() {

	summoner_count = 0;
	Summoner_Array = Array();

	chart = new google.visualization.LineChart(document.getElementById('chart_div'));
}

function updateGraphic(Summoner_Array)
{
	if(Summoner_Array[0] != null){
		var data = convertToDataGraphic(Summoner_Array);

		var options = {
			title: 'Last Games Results',
	  		//curveType: 'function',
	  		animation: {
	  			duration: 1000,
	  			easing: 'inAndOut'
	  		},
	  		legend: { position: 'right' },
	  		width: 900,
	  		height: 500,
	  	};

	  	drawGraphic(data, options);
	}else{
		chart.clearChart();
	}
}


function convertToDataGraphic(Summoner_Array){

	var data = new google.visualization.DataTable();
	data.addColumn('number', 'Matches');

	for ( var i = 0; i < Summoner_Array.length; i++ ) {
		data.addColumn('number', Summoner_Array[i][0]);
	}

	for(j=1; j<11; j++){
		data_row = Array();
		for(i=0; i<summoner_count+1; i++){
			if(i==0)
				data_row.push(j);
			else
				data_row.push(Summoner_Array[i-1][j]);
		}
		data.addRow(data_row);
	}

	return data;
}

function drawGraphic(data, options){
	chart.draw(data, options);
}

function Click_Menu(id_summoner, summoner_name){

	var removed = false;
	for(i=0; i<summoner_count; i++){
		if(Summoner_Array[i][0] == summoner_name)
		{
			Summoner_Array.splice(i,1);
			summoner_count--;
			removed = true;
			updateGraphic(Summoner_Array);
			break;
		}
	}
	if(!removed){
		$.ajax({
			type: "POST",
			data: { id_summoner:id_summoner },
			url: "stats_handle.php",
			dataType: "json",
			success: function(MatchStatus){
				Summoner_Array[summoner_count] = Array(summoner_name);
				for(var i=0; i<10; i++){
					if(MatchStatus[i])
						Summoner_Array[summoner_count].push(1);
					else
						Summoner_Array[summoner_count].push(0);
				}
				summoner_count++;
				updateGraphic(Summoner_Array);			
			},
		});
	}
	
}

</script>

<?php
$group_obj = new groups();
$query_groups = $group_obj->getAllGroups($_SESSION['id_user']);

echo '<div id="cssmenu"><ul>';

foreach($query_groups as $group){
	echo '
	<li><a href="#"><span>'.$group['group_name'].'</span></a>
	<ul>
	';
	$query_summoners = $group_obj->getAllSummoners($group['id_group']);
	if($query_summoners != null){
		foreach($query_summoners as $summoner){
			echo '
			<li class="menu_itens"><a href="#" onClick="Click_Menu('.$summoner['id_summoner'].",'".$summoner['summoner_name']."')".'"><span>'.$summoner['summoner_name'].'</span></a></li>
			';
		}
	}
	echo '
	</ul>
	</li>
	';
}

echo '</ul></div>';
?>

<div style="position: absolute; left: 300px; top: 100px;">
	<div id="chart_div"></div>
</div>

<?php
page::getFooter("LoL Stats");
?>