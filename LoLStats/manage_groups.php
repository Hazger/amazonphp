<?php 

include_once "classes/class.page.php";
include_once "classes/class.groups.php";

page::getHeader("LoL Stats");
?>
<br><br><br><br>

<?php

$group_obj = new Groups();

$query_groups = $group_obj->getAllGroups($_SESSION['id_user']);

foreach($query_groups as $group){
	echo '
	<div class="panel panel-default">
	<div class="panel-heading"><div>'.$group['group_name'].'</div><div>
					<form action="manage_groups_handle.php?code=2&group_name='.$group['group_name'].'" method="post">
						<input type="text" placeholder="New Summmoner" name="summoner_name" required>
						<button class="btn btn-lg btn-primary" type="submit">+</button>
					</form></div>
	</div>
	<table class="table">
	<tr>
	<th>Operations</th>
	<th>Summoner Name</th>
	<th>Elo</th>
	</tr>
	';
	$query_summoners = $group_obj->getAllSummoners($group['id_group']);
	if($query_summoners != null){
		foreach($query_summoners as $summoner){
			echo '
			<tr>
			<td>'."edit".'</td>
			<td>'.$summoner['summoner_name'].'</td>
			<td>'."Silver III".'</td>
			</tr>
			';
		}
	}
	echo '	
	</table>
	</div>
	';
}
?>

<form action="manage_groups_handle.php?code=1" method="post">
	<input type="text" placeholder="Group Name" name="group_name" maxlength="10" required>
	<button class="btn btn-lg btn-primary" type="submit">Add New Group</button>
</form>

<?php
page::getFooter("LoL Stats");
?>

