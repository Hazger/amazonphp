<?php 

class Request{
	var $key = "d24a3eb8-0b6a-4741-a554-22540457cff3";
	var $url = "https://br.api.pvp.net/api/lol/";
	var $region = "br/";
 
	public function callRequest($url){
		//return json_decode(file_get_contents($url), true);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0); //PRECISA DISSO se não não funciona
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$output = curl_exec($ch);
		$response_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);

		if($response_code == "200"){
			return null;
		}
		else{
			return json_decode($output, true);						
		}
	}

	public function getSummonerID($summonerName){
		$url = $this->url.$this->region."v1.4/summoner/by-name/".str_replace(" ", "%20", $summonerName)."?api_key=".$this->key;
		
		$json_reply = $this->callRequest($url, true);

		$summonerName = str_replace(" ", "", $summonerName);
		return $json_reply[$summonerName]["id"];
	}
	
	public function getSummonerInfo($summonerID){
		$url = $this->url.$this->region."v2.4/league/by-summoner/".$summonerID."/entry?api_key=".$this->key;
		$json_reply = $this->callRequest($url);
		
		if($json_reply == null){
			return null;
		}
		else{
			foreach ($json_reply[$summonerID][0] as $field => $values){
				$SummonerInfo[$field] = $values;
				if($field == "entries"){
					foreach ($SummonerInfo["entries"][0] as $itens => $valor)
						$SummonerInfo["entries"][$itens] = $valor;					
				}
			}
			unset($SummonerInfo["entries"][0]);
			return $SummonerInfo;
		}
	}
		
} //fim da classe Request

?>