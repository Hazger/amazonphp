var canvasWidth = 490;
var canvasHeight = 350;
var clickX = new Array();
var clickY = new Array();
var clickDrag = new Array();
var paint;
var canvas;
var context;

var corAtual = '#000';
var clickCor = new Array();

var tamanhoAtual = 5;
var clickTamanho = new Array();


//funcao inicial
function prepareCanvas() {
	
	//cria uma variavel canvasDiv que vai ser usada mais tarde
	var canvasDiv = $('#drawing-area');
	//cria um elemento canvas que eh a area de desenho
	canvas = $('<canvas>', {id: 'canvas'});
	$(canvas).attr("width", canvasWidth);
	$(canvas).attr("height", canvasHeight);	

	//coloca o canvas como filho da div
	$(canvasDiv).append(canvas);
	//transforma o canvas em uma area 2d....
	context = $(canvas)[0].getContext("2d");
	console.log(context);
	
	// Eventos do mouse
	// ----------------	
	//apertou o botao do mouse
	$('#canvas').mousedown(function(e) {
		//posicao do mouse
		//The HTMLElement.offsetLeft read-only method returns the number of pixels that the upper left corner of the current element is offset to the left within the HTMLElement.offsetParent node.
		//seta o mousex pra posicao x dentro do #canvas
		var mouseX = e.pageX - this.offsetLeft;
		//The HTMLElement.offsetTop read-only property returns the distance of the current element relative to the top of the offsetParent node.
		//seta o mousey pra posicao y dentro do #canvas
		var mouseY = e.pageY - this.offsetTop;
		//troca o painting pra true, isso quer dizer que o botao ta pressionado entao pintando
		paint = true;
		//chama o addClick
		addClick(mouseX, mouseY, false);
		//redesenha tudo
		redraw();
	});
	
	//moveu o mouse
	$('#canvas').mousemove(function(e){
		//se o paint for verdadeiro( botao do mouse estiver pressionado)
		if(paint){
			//passa o ponto do mouse com o dragging ativo (os pontos entre esse e o proximo ponto são uma linha, caso o mouse esteja se movendo muito rapdio)
			addClick(e.pageX - this.offsetLeft, e.pageY - this.offsetTop, true);
			//redesenha
			redraw();
		}
	});
	
	//soltou o botao do mouse 
	$('#canvas').mouseup(function(e){
		//quando solta o botao do mouse para de seta paint pra false (para de anotar os pontos)
		paint = false;
	  	redraw();
	});
	
	//mouse saiu da areaDesenho
	$('#canvas').mouseleave(function(e){
		//sem isso quando sai da area de desenho e volta pra ela sem soltar o mouse eh criada uma linha do lugar onde saiu pro lugar onde entrou
		paint = false;
	});	
}

function clearCanvas()
{
	//limpa o canvas
	context.clearRect(0, 0, canvasWidth, canvasHeight);
}

function clearDrawingCanvas()
{
	//clearCanvas();
	clickCor = new Array();
	clickDrag = new Array();
	clickTamanho = new Array();
	clickX = new Array();
	clickY = new Array();
}

//adiciona os pontos dos clicks 
function addClick(x, y, dragging)
{
	//empilha a posicao X no array clickX
	clickX.push(x);
	//empilha a posicao Y no array clickY
	clickY.push(y);
	//empilha o atributo dragging(se o mouse ta se movendo quando pegou esse ponto) no array clickDrag
	clickDrag.push(dragging);
	//empilha a cor atual
	clickCor.push(corAtual);
	clickTamanho.push(tamanhoAtual);
}

//redesenha a cada frame 
function redraw()
{
	//limpa o canvas
	clearCanvas();
		
	//quando duas linhas se encontram faz ela ser redonda (pra quando ta puxando o mouse muito rapido)
	context.lineJoin = "round";
	

	//percorre o array clickX
	for(var i=0; i < clickX.length; i++)
	{		
		//inicia uma linha "continua"
		context.beginPath();
		//se esse ponto foi criado enquanto o mouse se movia (eh uma linha continua)
		if(clickDrag[i] && i){
			//seta o ponto inicial na posicao clickX[i-1], clicky[i-1](o ponto anterior, se o mouse se move muito rapido os pontos sao pegos afastado entao isso faz eles virarem uma linha ao inves de pontos soltos)
			context.moveTo(clickX[i-1], clickY[i-1]);
		}else{
			//se o ponto nao foi criado enquanto o mouse se movia seta o ponto inicial pra clickx[i]-1(move a posicao 1 pixel pra esquerda) e clicky[i] (isso é feito pq um ponto sozinho nao cria a reta) [pog]
			context.moveTo(clickX[i]-1, clickY[i]);
		}
		//seta o ponto final pra posicao clickx[i], clicky[i]
		context.lineTo(clickX[i], clickY[i]);
		//finaliza a linha
		context.closePath();
		//troca a cor para a cor do clickCor[i]
		context.strokeStyle = clickCor[i];
		context.lineWidth = clickTamanho[i];
		//desenha a linha
		context.stroke();
	}
}