<?php 

require_once ("util/extra/connection.php");

class RiotApi{
	//API key
	const API_KEY = 'd24a3eb8-0b6a-4741-a554-22540457cff3';
	//var $key = "d24a3eb8-0b6a-4741-a554-22540457cff3";
	
	//URL pra ser enviados os requests	
	const API_URL_1_2 = 'https://br.api.pvp.net/api/lol/{region}/v1.2/';
	const API_URL_1_3 = 'https://br.api.pvp.net/api/lol/{region}/v1.3/';
	const API_URL_1_4 = 'https://br.api.pvp.net/api/lol/{region}/v1.4/';
	const API_URL_2_3 = 'https://br.api.pvp.net/api/lol/{region}/v2.3/';
	const API_URL_2_4 = 'https://br.api.pvp.net/api/lol/{region}/v2.4/';
	const API_URL_STATIC_1_2 = 'https://br.api.pvp.net/api/lol/static-data/{region}/v1.2/';

	//var $url = "https://br.api.pvp.net/api/lol/";
	
	//Limite de requests em 10 minutos
	const LONG_LIMIT_INTERVAL = 600;
	const LONG_LIMIT_RATE = 500;
	
	//limite de requests em 10 segundos
	const SHORT_LIMIT_INTERVAL = 10;
	const SHORT_LIMIT_RATE = 10;
	
	//cache
	const CACHE_LIFETIME_MINUTES = 60;
	const CACHE_ENABLED = true;	

	private $REGION;
	//var $output;
	//var $ch;
	
	public function __construct($region){
		$this->REGION = $region;

		$this->shortLimitQueue = new SplQueue();
		$this->longLimitQueue = new SplQueue();
	}
	
	public function getChampion(){		
		$call = 'champion';
		
		//coloca a url da API na chamada
		$call = self::API_URL_1_2 . $call;
		
		return $this->request($call);
	}
	
	public function getGame($id){
		//API VERSAO 1.3 PEGAR ESSA URL
		$call = 'game/by-summoner/' . $id . '/recent';
		
		//coloca a url da api na chamada
		$call = self::API_URL_1_3 . $call;
		
		return $this->request($call);
	}

	public function getLeague($id, $entry=null){
		//API VERSAO 2.4 PEGAR ESSA URL
		$call = 'league/by-summoner/' . $id . '/' . $entry;

		//coloca a url da api na chamada
		$call = self::API_URL_2_4 . $call;

		return $this->request($call);
	}
	
	public function getStatic($call=null, $id=null){
		//PEGAR ESSA URL DA ESTATICA
		$call = self::API_URL_STATIC_1_2 . $call . '/' . $id;
		
		return $this->request($call);
	}

	public function getAllStatic(){
		$call = self::API_URL_STATIC_1_2 . "champion?champData=all";

		return $this->request($call, true);
	}
	
	public function getStats($id, $option='summary'){
		$call = 'stats/by-summoner//' . $id . '/' . $option;;
		
		//coloca a url da api na chamada
		$call = selft::API_URL_1_3 . $call;
		
		return $this->request($call);
	}
	
	public function getSummoner($id, $option=null){
		$call = 'summoner/' . $id;
		switch($option){
			case 'masteries':
				//se a opcao for essa concatena isso no fim da $call
				$call .= '/masteries';
				break;
			case  'runes':
				//se a opcao for essa concatena isso no fim da $call
				$call .= '/runes';
				break;
			case 'name':
				//se a opcao for essa concatena isso no fim da $call
				$call .= '/name';
				break;
			default:
				//se for nulo nao faz nada
				break;
		}
		
		//coloca a url da api na chamada
		$call = self::API_URL_1_4 . $call;
		
		return $this->request($call);
	}
	
	public function getTeam($id){
		//API VERSAO 2.3 PRECISA PEGAR ISSO CERTO DEPOIS
		$call = 'team/by-summoner/' . $id;
		
		//coloca a url da api na chamada
		$call = self::API_URL_2_3 . $call;
		
		return $this->request($call);
	}
	
	//Essa chamada nao sei se vai ser usada
	public function getChallenger(){
		$call = 'league/challenger?type=RANKED_SOLO_5x5';
		
		$call = self::API_URL_2_3 . $call;
		
		return $this->request($call, true);
	}
	
	//essa chamada nao sei se vai ser usada
	public function getSummonerID($name){
		$summoner = $this->getSummonerByName($name);
		return $summoner[$name]["id"];
	}
	
	//essa chamada nao sei se vai ser usada
	public function getSummonerByName($name){
		//uso o rawurlencode para encodar os caracteres especiais
		$call = 'summoner/by-name/' . strtolower($name);
		
		$call = self::API_URL_1_4 . $call;
		
		return $this->request($call);
	}
	
	//essa funcao provavelmente vou modificar
	private function updateLimitQueue($queue, $interval, $call_limit){
		while(!$queue->isEmpty()){
			/*
			 Pode acontecer 3 casos
			 1: Tem horarios(timestamps) na fila fora da janela  de intervalo o que significa que os requests associados com esse timestamp
			 foram a tempo suficiente e podem ser removidos da fila
			 2: O limite de chamadas estorou. Nesse caso programa da sleep ate se liberar
			 3: a janela ainda não a cheia entao o programa pode continuar
			*/
			
			$timeSinceOldest = time() - $queue->bottom();
			//pega o primeiro da fila e ve quanto tempo ja passou desde que ele entrou na fila(ele é o mais antigo da fila)
			
			//tira da fila se for mais antigo que o intervalo
			if ($timeSinceOldest > $interval){
				$queue->dequeue();
			}
			//checa para ver se o limite não vai explodir; se sim o programa dorme ate o limite voltar pro aceitavel
			elseif($queue->count() >= $call_limit){
				echo("Aguardando " .( $interval - $timeSinceOldest +1). "segundos \n");
				sleep($interval - $timeSinceOldest+1);
			}
			//se nao passa e continua executando o programa
			else {
				break;
			}
		}
		//adiciona o tempo na no fim da queue
		$queue->enqueue(time());		
	}

	private function request($call, $otherQueries=false){
		//coloca o controle de pedidos aqui
		//confere o limite de queues
		$this->updateLimitQueue($this->longLimitQueue, self::LONG_LIMIT_INTERVAL, self::LONG_LIMIT_RATE);
		$this->updateLimitQueue($this->shortLimitQueue, self::SHORT_LIMIT_INTERVAL, self::SHORT_LIMIT_RATE);

		//format the full URL
		$url = $this->format_url($call, $otherQueries);
		
		//echo($url);
		//echo("<br>");

		//caching
		if(self::CACHE_ENABLED){
			$cacheFile = 'cache/' . md5($url);

			if(file_exists($cacheFile)){
				$fh = fopen($cacheFile, 'r');
				$cacheTime = trim(fgets($fh));

				//if data was cache recently return cached data
				if ($cacheTime > strtotime('-'. self::CACHE_LIFETIME_MINUTES . ' minutes')){
					$data = fread($fh, filesize($cacheFile));
					return $data;
				}

				//else delete cache file
				fclose($fh);
				unlink($cacheFile);				
			}
		}

		//chama a api e retorna o resultado
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0); //PRECISA DISSO se não não funciona
		$result = curl_exec($ch);
		curl_close($ch);		

		if(self::CACHE_ENABLED){
			//cria o arquivo de cache
			//echo($cacheFile);
			file_put_contents($cacheFile, time() . "\n" . $result);			
		}
		//echo("<br>ali vai o resultado<br>");
		//echo($result); //resultado ta vindo em branco
		return $result;
	}

	private function format_url($call, $otherQueries=false){
		if($otherQueries){
			return str_replace('{region}', $this->REGION, $call) . '&api_key=' . self::API_KEY;			
		}
		return str_replace('{region}', $this->REGION, $call) . '?api_key=' . self::API_KEY;
	}

	public function debug($message){
		echo "<pre>";
		print_r($message);
		echo "</pre>";
	}
} //fim da classe Request

?>
