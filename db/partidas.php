<?php

	require_once ("util/extra/connection.php");

	//Includes
	include_once "util/class/pagina.php";	
	include_once "util/class/riotapi.php";
	include_once "util/class/parser.php";

	//instancia as classes
	$pagina = new Pagina;	
	$riotApi = new RiotApi('br');
	$parser = new Parser();

	$pagina->getHeader("Partidas");

	//$result = $riotApi->getGame($riotApi->getSummonerByName($_GET["summonerName"]));
	$result = $riotApi->getSummonerByName($_GET["summonerName"]);
	$result = $parser->GetSummonerIdByName($result, $_GET["summonerName"]);
	
	$result = $riotApi->getGame($result);
	$parser->parseGames($result);

	//var_dump($riotApi->getSummonerByName($_GET["summonerName"]));
	//$parser->parseGames($result);
?>